import java.util.*;

public class Die 
{
  private int pips;
  private Random rand;

  public Die()
  {
    this.pips = 1;
    this.rand = new Random();
  }
  public int getPips()
  {
    return this.pips;
  }
  public int roll()
  {
    return pips = rand.nextInt(6)+1;
  }
  public String toString()
  {
    return "The die shows " + this.pips + " pips";
  }
}