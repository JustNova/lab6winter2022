import java.util.Scanner;
public class ShutTheBox
{
    public static void main(String[]Args)
    {
       Scanner in = new Scanner(System.in);
       boolean gameOver = false;
       boolean playAgain = true;
       int p1Wins = 0;
       int p2Wins = 0;
       Board newBoard = new Board();
       while(!playAgain);
        {
        System.out.println("Welcome to Shut The Box\n"); 
        while(!gameOver)
        {
            System.out.println("Player 1's turn\n");
            System.out.println(newBoard.toString());
            if (newBoard.playATurn())
            {
                System.out.println("Player 2 wins\n");
                p2Wins++;
                System.out.println("Do you want to play again?\ny or n");
                String again = in.nextLine();
                again.toLowerCase();
                if (again.equals("n"))
                {
                    playAgain = false;
                    gameOver = true;
                    System.out.println("Player 1 won " + p1Wins + " times\nAnd Player 2 won " + p2Wins + " times");
                }
                else
                {
                    playAgain = true;
                    newBoard = new Board();
                }

            }
            else
            {
                System.out.println("Player 2's turn\n");
                System.out.println(newBoard.toString());
                if (newBoard.playATurn())
                {
                    System.out.println("Player 1 wins\n");
                    p1Wins++;
                    
                    System.out.println("Do you want to play again?\ny or n");
                    String again = in.nextLine();
                    again.toLowerCase();
                    if (again.equals("n"))
                    {
                        playAgain = false;
                        gameOver = true;
                        System.out.println("Player 1 won " + p1Wins + " times\nAnd Player 2 won " + p2Wins + " times");
                    }
                    else
                    {
                        playAgain = true;
                        newBoard = new Board();
                    }
                }
            }
        }
       }
       in.close();
    }
}