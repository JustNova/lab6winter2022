public class Board 
{
  private Die die1;
  private Die die2;
  private boolean[] closedTiles;

  public Board()
  {
    this.die1 = new Die();
    this.die2 = new Die();
    this.closedTiles = new boolean[12];
  }
  public String toString()
  {
    String tiles = "";
    for (int i = 0; i < closedTiles.length; i++) {
      if (!closedTiles[i])
      {
        tiles = tiles + (1+i) + " ";
      }
      else
      {
        tiles = tiles + "X ";
      }
    }
    return tiles;
  }
  public boolean playATurn()
  {
    die1.roll();
    die2.roll();
    System.out.println(die1);
    System.out.println(die2);
    int sumOfPips = die1.getPips() + die2.getPips();
    if (!closedTiles[sumOfPips-1])
    {
      closedTiles[sumOfPips-1] = true;
      System.out.println("Closing tile " + sumOfPips + "!\n");
      return false;
    }
    else
    {
      System.out.println("Tile " + sumOfPips + " is already closed\n");
      return true;
    }
  }
}

